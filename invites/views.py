# coding: utf-8
from collections import Counter

from flask import (Blueprint, redirect, url_for, make_response, request, flash,
                   Response)

from main.utils import render_to, staff_only

from .forms import SubscribeForm, InviteForm
from .models import Invite, Subscribe


invites_app = Blueprint('invites', __name__, template_folder='templates')


@invites_app.route('/invite/<inv:invite>', methods=['GET', 'POST'])
@render_to('invites/invite.jinja')
def invite_page(invite):
    """ Страница подписки
    """
    if invite.is_full:
        flash(u'Вы уже подписались')
        return redirect(url_for('main.index'))

    form = SubscribeForm(invite=invite)
    subscribed = request.cookies.get(invite.uuid)

    if subscribed:
        return locals()

    if form.validate_on_submit():
        sub = Subscribe(email=form.email.data, invite=invite.key)
        sub.put()
        subscribed = True
        COOKIES = ((invite.uuid, invite.uuid),)

    return locals()


@invites_app.route('/admin/invites', methods=['GET', 'POST'])
@render_to('invites/admin.jinja')
@staff_only
def admin_page():
    """ Админка.
    """
    form = InviteForm()

    if form.validate_on_submit():
        inv = Invite(text=form.text.data, limit=form.limit.data)
        inv.put()
        flash(u'Добавлено приглашение для: {}'.format(inv.text))
        return redirect(url_for('invites.admin_page'))

    order = request.args.get('o')
    invites = Invite.all_ordered(order)

    subs = Subscribe.query(projection=[Subscribe.email_domain]).fetch()
    subs_stats = Counter((x.email_domain for x in subs))
    return locals()


@invites_app.route('/admin/subs/grab', methods=['GET'])
@staff_only
def admin_subs_grab():
    """ Скачка файла со списком адресов.
    """
    emails = (u'{}\n'.format(s.email) for s in
              Subscribe.query(projection=[Subscribe.email],
                              distinct=True).fetch())

    return Response(emails, mimetype='text/plain')
