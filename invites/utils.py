# coding: utf-8
from werkzeug.routing import UUIDConverter, ValidationError

from .models import Invite

__ALL__ = ('InviteConverter',)


class InviteConverter(UUIDConverter):
    """ Конвертер, который преобразует uuid в строке адреса приглашения,
    в инстанс этого приглашения, если оно существует.
    """
    def to_python(self, value):
        inv = Invite.track_and_get(value)
        if inv is None:
            raise ValidationError()

        return inv

    def to_url(self, value):
        return unicode(value.uuid)
