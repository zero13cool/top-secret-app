# coding: utf-8
import uuid

from google.appengine.ext import ndb

__ALL__ = ('Invite', 'Subscribe')


class Invite(ndb.Model):
    """ Модель приглашения.
    """
    uuid = ndb.StringProperty()
    text = ndb.TextProperty(required=True)
    limit = ndb.IntegerProperty(default=1)
    created = ndb.DateTimeProperty(auto_now_add=True)

    # Счетчики
    emails_count = ndb.IntegerProperty(default=0)
    views = ndb.IntegerProperty(default=0)

    def _pre_put_hook(self):
        if not self.uuid:
            self.uuid = unicode(uuid.uuid1())

    DEFAULT_ORDER_BY = '-created'
    ORDER_BY_FIELDS = ('created', 'emails_count', 'views',
                       DEFAULT_ORDER_BY, '-emails_count', '-views')

    @classmethod
    def all_ordered(cls, field):
        """ Выдает отсортированную выборку.
        """
        if field not in cls.ORDER_BY_FIELDS:
            field = cls.DEFAULT_ORDER_BY

        prop = getattr(cls, field[field.startswith('-'):])

        if field.startswith('-'):
            prop = prop.__neg__()

        return cls.query().order(prop)

    @classmethod
    def get_by_uuid(cls, uuid):
        """ Получение по UUID.
        """
        return cls.query(cls.uuid == uuid).get()

    @staticmethod
    def track_and_get(uuid):
        """ Получаем приглашение по его uuid и трекаем это, как просмотр.
        """
        inv = Invite.get_by_uuid(uuid)

        @ndb.transactional
        def incr(key):
            obj = key.get()
            obj.views += 1
            obj.put()

        if inv:
            incr(inv.key)

        return inv

    def is_already_exists(self, email):
        """ По-хорошему надо unique_together еще навернуть.
        """
        return Subscribe.query(Subscribe.invite == self.key,
                               Subscribe.email == email).get()

    @property
    def is_full(self):
        return self.emails_count >= self.limit


class Subscribe(ndb.Model):
    """ Подписка.
    """
    email = ndb.StringProperty(required=True)
    email_domain = ndb.StringProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)

    invite = ndb.KeyProperty(kind=Invite)

    def _pre_put_hook(self):
        self.email_domain = self.email.split('@')[-1]

    def _post_put_hook(self, *args, **kwargs):
        @ndb.transactional
        def incr():
            """ Увеличивает счетчик подписчиков.
            """
            obj = self.invite.get()
            obj.emails_count += 1
            obj.put()

        incr()
