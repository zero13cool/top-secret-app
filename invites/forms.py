# coding: utf-8
from flask_wtf import Form
from wtforms.fields import SubmitField, StringField, IntegerField
from wtforms.fields.html5 import EmailField
from wtforms.validators import Email, ValidationError


__ALL__ = ('InviteForm', 'SubscribeForm')


class InviteForm(Form):
    """ Форма создания нового приглашения.
    """
    text = StringField(u'Приглашения для')
    limit = IntegerField(u'Лимит подписок')
    add = SubmitField(u'Добавить')


class SubscribeForm(Form):
    """ Форма для подписки на приглашение.
    """
    def __init__(self, invite, *args, **kwargs):
        self.invite = invite
        super(SubscribeForm, self).__init__(*args, **kwargs)

    email = EmailField(u'Эл. почта', [Email()])
    subscribe = SubmitField(u'Подписаться!')

    def validate_email(self, field):
        email = field.data

        if self.invite.is_already_exists(email):
            raise ValidationError(u'Вы уже подписались')
