# coding: utf-8
import unittest

from google.appengine.api import memcache
from google.appengine.ext import ndb
from google.appengine.ext import testbed

from flask import url_for, request
from flask.ext.testing import TestCase

from app import app
from invites.models import Invite, Subscribe
from invites.forms import SubscribeForm

__ALL__ = ('InvitePageTestCase',)


class InvitePageTestCase(TestCase):
    """ Тестирование страницы приглашения.
    """
    def create_app(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        return app

    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()

        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        ndb.get_context().clear_cache()

        self.invite = Invite(text='Joe Dow', limit=10)
        self.invite.put()
        self.url = url_for('invites.invite_page', invite=self.invite)

    def tearDown(self):
        self.testbed.deactivate()

    def test_page_opened(self):
        """ Страница открывается, просмотры считаются, форма на странице.
        """
        response = self.client.get(self.url)

        self.assert_template_used('invites/invite.jinja')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.invite.key.get().views, 1)
        self.assertIsInstance(self.get_context_variable('form'), SubscribeForm)
        self.assertIn('<form', response.data)

    def test_valid_sub(self):
        """ Подписка происходит, форма не отображается, счетчики увеличиваются.
        """
        valid_data = {'email': 'test@test.ru'}
        response = self.client.post(self.url, data=valid_data)

        self.assertEqual(response.status_code, 200)
        self.assertNotIn('<form', response.data)
        self.assertEqual(self.invite.key.get().emails_count, 1)
        self.assertTrue(self.invite.is_already_exists(valid_data['email']))

    def test_empty_email(self):
        """ Пустая почта не проходит.
        """
        invalid_data = {'email': ''}
        self._test_invalid_post(invalid_data)

    def test_invalid_email(self):
        """ Некорретная почта - не проходит.
        """
        invalid_data = {'email': 'hello world'}
        self._test_invalid_post(invalid_data)

    def test_registered_email(self):
        """ Зарегистрированная почта второй раз не проходит (не смотря на куку)
        """
        valid_data = {'email': 'test@test.ru'}
        sub = Subscribe(email=valid_data['email'], invite=self.invite.key)
        sub.put()

        response = self.client.post(self.url, data=valid_data)
        self.assertEqual(response.status_code, 200)
        self.assertIn('<form', response.data)
        self.assertEqual(self.invite.key.get().emails_count, 1)
        self.assertTrue(self.invite.is_already_exists(valid_data['email']))
        self.assertTrue(self.get_context_variable('form').errors)

    def test_limit_reached(self):
        """ При превышении лимита подписок - редирект на главную.
        """
        self.invite.emails_count = self.invite.views
        self.invite.put()

        response = self.client.get(self.url)
        self.assertTrue(response.status_code, 302)

    def _test_invalid_post(self, data):
        """ Общая функция проверки невалидного поста.
        """
        response = self.client.post(self.url, data=data)

        self.assertEqual(response.status_code, 200)
        self.assertIn('<form', response.data)
        self.assertEqual(self.invite.key.get().emails_count, 0)
        self.assertFalse(self.invite.is_already_exists(data['email']))
        self.assertTrue(self.get_context_variable('form').errors)
