.PHONY: clean

clean:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +


install: clean
	rm -rf lib/*
	pip install -r requirements.txt -t lib

run: clean
	dev_appserver.py .

test: clean
	python tests.py
