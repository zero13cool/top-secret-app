# coding: utf-8
from google.appengine.api import users

from flask import Flask, redirect, request, g
from flask.ext.bootstrap import Bootstrap

app = Flask(__name__,
            instance_relative_config=True)

bootstrap = Bootstrap(app)

app.config.from_object('settings')


def register_all(app):
    from invites import invites_app
    from main import main_app

    from invites.utils import InviteConverter
    app.url_map.converters['inv'] = InviteConverter

    app.register_blueprint(invites_app)
    app.register_blueprint(main_app)

register_all(app)
