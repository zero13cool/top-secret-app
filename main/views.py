# coding: utf-8
from flask import Blueprint, render_template

from main.utils import render_to, staff_only


main_app = Blueprint('main', __name__, template_folder='templates')


@main_app.route('/')
@render_to()
def index():
    return locals()


@main_app.route('/admin')
@render_to('admin.jinja')
@staff_only
def admin_page():
    """ Админка.
    """
    return locals()
