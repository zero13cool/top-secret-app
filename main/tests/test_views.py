# coding: utf-8
from flask import url_for, request

from main.tests.base import TestCase

__ALL__ = ('IndexPageTestCase', 'AdminPageTestCase')


class IndexPageTestCase(TestCase):
    """ Тест-кейс главной страницы.
    """
    def test_opened(self):
        url = url_for('main.index')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)


class AdminPageTestCase(TestCase):
    """ Тест-кейс для административной страницы.
    """
    def setUp(self):
        super(AdminPageTestCase, self).setUp()
        self.url = url_for('main.admin_page')
        self.testbed.init_user_stub()

    def login(self, email='user@example.com', id='123', is_admin=False):
        self.testbed.setup_env(
            user_email=email,
            user_id=id,
            user_is_admin='1' if is_admin else '0',
            overwrite=True)

    def test_anon_not_allow(self):
        """ Аноним не имеет доступа к разделу.
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_not_admin_not_allow(self):
        """ Не администратор также не имеет доступа.
        """
        self.login()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_admin_allow(self):
        """ Администратор может открыть страницу.
        """
        self.login(is_admin=True)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assert_template_used('admin.jinja')
