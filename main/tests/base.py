# coding: utf-8
from flask.ext.testing import TestCase as BaseTestCase

from google.appengine.api import memcache
from google.appengine.ext import ndb
from google.appengine.ext import testbed

from app import app

__ALL__ = ('TestCase',)


class TestCase(BaseTestCase):
    """ Базовый тест кейс с необходимой инициализацией для GAE и Flask.
    """
    def create_app(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        return app

    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()

        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        ndb.get_context().clear_cache()

    def tearDown(self):
        self.testbed.deactivate()
