# coding: utf-8
from functools import wraps

from google.appengine.api import users

from flask import redirect, request, render_template, make_response
from werkzeug.wrappers import BaseResponse

__ALL__ = ('login_required', 'render_to')


def staff_only(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        user = users.get_current_user()
        if not user or not users.is_current_user_admin():
            return redirect(users.create_login_url(request.url))
        return func(*args, **kwargs)
    return decorated_view


def render_to(template=None):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            status_code = 200
            ctx = f(*args, **kwargs) or {}

            template_name = template
            if template_name is None:
                template_name = f.__name__ + '.jinja'

            if isinstance(ctx, BaseResponse):
                return ctx

            if isinstance(ctx, tuple):
                ctx, status_code = ctx

            COOKIES = ctx.pop('COOKIES', None)
            if COOKIES:
                rendered = render_template(template_name, **ctx)
                response = make_response(rendered, status_code)

                for k, v in COOKIES:
                    response.set_cookie(k, v)

                return response

            return render_template(template_name, **ctx), status_code
        return decorated_function
    return decorator
